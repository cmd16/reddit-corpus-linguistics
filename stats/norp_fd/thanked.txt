#Word types: 1047
#Word tokens: 9882
#Search results: 0
1	1962	Japanese	
2	710	Pelikan	
3	680	Chinese	
4	635	Safari	
5	380	German	
6	299	Pelikans	
7	287	European	
8	224	polish	
9	181	American	
10	149	Montblanc	
11	140	japanese	
12	129	Jinhao	
13	128	Clairefontaine	
14	125	Spencerian	
15	121	Indian	
16	108	Italian	
17	107	French	
18	103	chinese	
19	100	Western	
20	95	Canadian	
21	91	Australian	
22	70	Rohrer	
23	65	Asian	
24	58	Falcon	
25	54	Broad	
26	53	Pelikan 4001	
27	48	Scabiosa	
28	47	Watermans	
29	45	Irish	
30	44	german	
31	43	Dutch	
32	41	British	
33	41	Vacumatics	
34	41	Akkerman	
35	34	Sailor	
36	29	Taiwanese	
37	28	Moleskine	
38	28	Korean	
39	27	Bernanke	
40	26	safari	
41	26	french	
42	23	English	
43	22	Loom	
44	21	North African	
45	21	indian	
46	20	Platinums	
47	20	350ml	
48	19	asian	
49	19	Ottoman	
50	18	Snorkel	
51	17	Fine	
52	17	Spanish	
53	17	european	
54	17	Russian	
55	17	Americans	
56	15	Polish	
57	14	Soviet	
58	14	Greek	
59	13	spencerian	
60	13	Google	
61	13	A4	
62	13	FA	
63	12	Ahabs	
64	12	Waterman	
65	12	North American	
66	12	american	
67	12	Germans	
68	12	Latin	
69	12	PCH912	
70	12	Bungubox	
71	11	LAMY	
72	11	Jinhao x750	
73	11	Jinhao 159	
74	11	x450	
75	11	Franklin	
76	11	Indus	
77	10	JoWo	
78	10	swab	
79	10	italian	
80	10	Persian	
81	10	Lamy Safari	
82	10	Pelican	
83	10	Ginza	
84	10	Jowo	
85	10	Karas	
86	10	KoP	
87	9	Canadians	
88	9	Norwegian	
89	9	Resin	
90	9	USPS	
91	9	Lanbitou	
92	9	Zoom	
93	9	Kuretake	
94	8	Borealis	
95	8	Prussian	
96	8	ESSRI	
97	8	Pablo	
98	7	mech	
99	7	Italic	
100	7	Swan	
101	7	Sailors	
102	7	Arabic	
103	7	Taranis	
104	7	Stipula	
105	7	pelikans	
106	7	PayPal	
107	7	Tyrian	
108	6	Italians	
109	6	Sagaris	
110	6	Danish	
111	6	Penman	
112	6	Swiss	
113	6	Mediums	
114	6	Instagram	
115	6	Realo	
116	6	Sonnets	
117	6	Pilots	
118	6	3776	
119	6	EoC	
120	6	Belgian	
121	6	Aquamarine	
122	6	non-Japanese	
123	6	Moonman	
124	5	soviet	
125	5	Australians	
126	5	arabic	
127	5	dutch	
128	5	Ones	
129	5	Verdigris	
130	5	Vietnamese	
131	5	Nib	
132	5	Jewish	
133	5	Barnes	
134	5	Noble	
135	5	D.	
136	5	EXACT	
137	5	Jinhao x450	
138	5	Sunshine	
139	5	AL	
140	5	Orange	
141	5	Duofolds	
142	5	Italix	
143	5	Platinum	
144	5	Caran	
145	5	Aubergine	
146	5	Stylist	
147	5	FC	
148	5	Aerometrics	
149	5	Austrian	
150	5	Libertarian	
151	5	irish	
152	5	Estie	
153	5	J.Herbin	
154	5	Honey	
155	5	Japenese	
156	5	Kakuno	
157	4	Malaysian	
158	4	Sonnet	
159	4	Swans	
160	4	the Lamy Safari	
161	4	Swedish	
162	4	Finish	
163	4	Eastern	
164	4	Serbian	
165	4	Souverans	
166	4	Nazi	
167	4	Baystate	
168	4	Majestic	
169	4	Platinum 3776	
170	4	Mexican	
171	4	Souveran	
172	4	Pakistani	
173	4	Catholic	
174	4	EdC	
175	4	Danitrio	
176	4	Celluloid	
177	4	Scottish	
178	4	varnish	
179	4	Medium	
180	4	Victorian	
181	4	Singaporean	
182	4	longish	
183	4	Reddit	
184	4	Scotch	
185	4	Z55	
186	4	Martian	
187	3	Wearevers	
188	3	mexican	
189	3	Black Swans	
190	3	Calligraphy	
191	3	greek	
192	3	Eagle	
193	3	Altoids	
194	3	Manganese	
195	3	Conklin	
196	3	9xxx	
197	3	australian	
198	3	pelican	
199	3	Craftsman	
200	3	Pelikan 140	
201	3	j&amp;q=&amp;esrc	
202	3	Artista	
203	3	LAMY Safari	
204	3	Pine	
205	3	Sentinel	
206	3	Shaker	
207	3	Urbans	
208	3	Levenger	
209	3	Junghans	
210	3	swans	
211	3	Yankee	
212	3	Mini	
213	3	yellowish	
214	3	Iconic	
215	3	Christian	
216	3	jinhao x750	
217	3	Thai	
218	3	Kaweco	
219	3	jinhao	
220	3	Chromium	
221	3	DL	
222	3	Al	
223	3	Diplomat	
224	3	Imgur	
225	3	F.	
226	3	Japanese Fs	
227	3	english	
228	3	Japanese Fines	
229	3	Whitelines	
230	3	Czech	
231	3	Masuyama	
232	3	Turkish	
233	3	Mandarin	
234	3	Brazilian	
235	3	Yamabudo	
236	3	accordian	
237	3	Bavarian	
238	3	Hawaiian	
239	3	Horizontal	
240	3	Jinhao 911	
241	3	P66	
242	2	Richards	
243	2	Specerian	
244	2	Jeans	
245	2	Bleedthrough	
246	2	Bulletproof	
247	2	Polar	
248	2	Cavalier	
249	2	north african	
250	2	Pelikan M215	
251	2	Nordic	
252	2	swiss	
253	2	non-US	
254	2	Piston	
255	2	greens	
256	2	lamy safari	
257	2	Brownian	
258	2	Aeromatic	
259	2	swan	
260	2	TWSBI	
261	2	Yonge	
262	2	Sean	
263	2	Lamy-Safari	
264	2	Schmidt	
265	2	Tangerine	
266	2	blackish	
267	2	/r	
268	2	Nazis	
269	2	Silver	
270	2	Mech	
271	2	latin	
272	2	Waterproof	
273	2	Lange	
274	2	Melbournian	
275	2	Spenserian	
276	2	Pelikan 1000	
277	2	South Korean	
278	2	x750	
279	2	blueish	
280	2	100ml	
281	2	Heros	
282	2	Jinhao 599s	
283	2	communist	
284	2	non-American	
285	2	1.1	
286	2	Kligner	
287	2	Gurus	
288	2	Oriental	
289	2	Bullet	
290	2	british	
291	2	Ondoro	
292	2	Nimosine	
293	2	F Japanese	
294	2	Eco	
295	2	sans	
296	2	Europeans	
297	2	Pinkish	
298	2	Democrat	
299	2	All-American	
300	2	Han	
301	2	Yukitsubaki	
302	2	I.	
303	2	AutoMod	
304	2	Shimmertastics	
305	2	2cents	
306	2	Meisturstuck	
307	2	Pharmacist	
308	2	prussian	
309	2	Denim	
310	2	Townsend	
311	2	A7	
312	2	Rhodori	
313	2	Irori	
314	2	Pelikan m200	
315	2	Montblanc Irish	
316	2	Lebanese	
317	2	Music	
318	2	Maltese	
319	2	Jinhao 599	
320	2	Corundum	
321	2	East Asian	
322	2	XXXF	
323	2	Spencarian	
324	2	the Montblanc 149	
325	2	Bluish	
326	2	C823	
327	2	http://www.gouletpens.com/ink-sample-packages/c/23	
328	2	Pharisees	
329	2	Durograph	
330	2	Greens	
331	2	Wish	
332	2	Gist	
333	2	eastern	
334	2	Dorcus	
335	2	Slim	
336	2	Preludes	
337	2	Stellar	
338	2	Kyobo	
339	2	Appelboom	
340	2	non-IG	
341	2	nib - one	
342	2	KS	
343	2	Cousin	
344	2	gallery&amp;ga_search_query	
345	2	Fred	
346	2	Pelikan Souverän	
347	2	Heritance	
348	2	Cheers	
349	2	Salix	
350	2	KWZI	
351	2	Arabian	
352	2	Codd	
353	2	Palladiums	
354	2	non-japanese	
355	2	czech	
356	2	Adelaide	
357	2	Reddish	
358	2	Jinhao 992	
359	2	Ansers	
360	2	Bromfield	
361	2	Texans	
362	2	Swab	
363	2	Jowos	
364	2	Pelikan m805	
365	2	Peans	
366	2	Pelikan M200	
367	2	Shikiori	
368	2	Monteverde	
369	2	323s	
370	2	Soooooo	
371	2	KOP	
372	2	Zafeiriou	
373	2	Parkers	
374	2	Labans	
375	2	a Franklin Christoph	
376	2	marxist	
377	2	SIMPLO	
378	2	Moonmans	
379	2	Croatian	
380	1	waterman+Phileas&amp;aq=0&amp;aqi	
381	1	MaeSwan	
382	1	LH	
383	1	Statists	
384	1	Russians	
385	1	8k/12k	
386	1	Native Americans	
387	1	BayState	
388	1	cup	
389	1	egyptians	
390	1	3rd	
391	1	CP-1	
392	1	g.	
393	1	Romanian	
394	1	Gentleman	
395	1	Executive	
396	1	Floridian	
397	1	Kataish	
398	1	Non-German	
399	1	stylos.de	
400	1	the Franklin Christoph	
401	1	120gm	
402	1	pics](http://newtonpens.wordpress.com	
403	1	chrome&amp;ie	
404	1	Arsenic	
405	1	univ&amp;sa	
406	1	DR - Japanese	
407	1	pelikan	
408	1	JUUUUST	
409	1	TM	
410	1	Mendelian	
411	1	Blackbirds	
412	1	ElAl	
413	1	http://www.reddit.com/r/notebooks/comments/1qyklw/podrick_my_latest_notebook_hack/	
414	1	Parkerish	
415	1	Scrikss	
416	1	Metropolitian	
417	1	San Fran	
418	1	Wonderpens	
419	1	Herbin	
420	1	Singularities	
421	1	Gothic	
422	1	ur hnd glidng ovr	
423	1	8.25x11.25	
424	1	Phoenix	
425	1	Slowly	
426	1	trollish	
427	1	Dandyism	
428	1	Coconut	
429	1	ginza	
430	1	western	
431	1	GORGEOUS	
432	1	the German	
433	1	Crazy	
434	1	albanian	
435	1	nigerian	
436	1	Arab	
437	1	Papakuma Japanese	
438	1	Approx	
439	1	sb&amp;q	
440	1	how%20to%20draw%203d%20shapes%20with%20shading&amp;revid=1784707504&amp;rls	
441	1	Swatch	
442	1	gunkan	
443	1	Ikemiyagi	
444	1	Punch	
445	1	U Indian	
446	1	jean	
447	1	JetPens	
448	1	Arrow	
449	1	Hungarian	
450	1	Conservative	
451	1	Venus	
452	1	UTF8&amp;qid=1411700750&amp;sr=8-1&amp;keywords	
453	1	https://www.youtube.com/watch?v=g7F5Vbz1nTg	
454	1	Stopped	
455	1	please/?p=2691497	
456	1	UTF8&amp;qid=1412733224&amp;sr=8-1&amp;keywords	
457	1	a F Safari	
458	1	World Lux	
459	1	Iraqi	
460	1	Someones	
461	1	sagaris	
462	1	Kaweco-AL-Sport	
463	1	Elysium	
464	1	http://www.jetpens.com/Kaweco-Classic-Sport-Fountain-Pen-Extra-Fine-Nib-Clear-Body/pd/7508	
465	1	rKIMaEuScarhH57I7D7D0g&amp;bvm	
466	1	Fiancée	
467	1	Olympio	
468	1	brazilian	
469	1	Diaphragm	
470	1	Super5	
471	1	500px	
472	1	UTF8&amp;qid=1418738724&amp;sr=8-	
473	1	M1000 Pelikans	
474	1	Ieyasu	
475	1	hebrew/arabic	
476	1	Spicypenis	
477	1	https://www.youtube.com/watch?v=wgCuFicdUS4	
478	1	InkDrop	
479	1	300pp	
480	1	tyrian	
481	1	Studio Blue Crab	
482	1	Scientific American	
483	1	Plebeians	
484	1	MontBlanc	
485	1	Parthenon	
486	1	Patrician	
487	1	Giardino	
488	1	japanese-fp/#entry3062363	
489	1	http://imgur.com/bx2sZfB	
490	1	http://imgur.com/a/P730f	
491	1	a Fine Safari	
492	1	^so ^bungbox	
493	1	thans	
494	1	m2xx	
495	1	F. Japanese	
496	1	Isellpens	
497	1	Leuchtturm 100gm	
498	1	Quink	
499	1	Harlequins	
500	1	Feds	
501	1	Sleuth	
502	1	Copic	
503	1	Pelikan Aventurine	
504	1	a M Japanese	
505	1	Skytree	
506	1	Ukrainian	
507	1	Snorks	
508	1	hardish	
509	1	Macbook	
510	1	Clarfontaine	
511	1	Aventurine	
512	1	Scheveningen	
513	1	Handsome	
514	1	yank	
515	1	Paino	
516	1	Joy	
517	1	XF	
518	1	~£225-250	
519	1	Sheaffers-	
520	1	Scrumpy	
521	1	Momiji	
522	1	Bulgarian	
523	1	item20f43e4c0e	
524	1	Snorkel](https://edjelley.files.wordpress.com/2013/07	
525	1	Nijmegen	
526	1	non-Pelikan	
527	1	Penroom	
528	1	Aesthetics	
529	1	Amsterdam Akkerman	
530	1	Shaeffers	
531	1	JUNGHANS](http://www.junghans.de	
532	1	Christians	
533	1	Goulet	
534	1	Esteem	
535	1	faroese	
536	1	Sustainable	
537	1	Essentials	
538	1	XG	
539	1	m2XX	
540	1	Auchentoshan	
541	1	Montblanc-British	
542	1	400s	
543	1	250th	
544	1	Chezh	
545	1	matte(ish	
546	1	Fountainpens	
547	1	a W. German 150	
548	1	Minuskin	
549	1	scottish	
550	1	Taoism	
551	1	Stratford	
552	1	Platinum Japanese	
553	1	a F nib	
554	1	non-chinese	
555	1	https://www.youtube.com/watch?v=OxH5VS9BeO8&amp;index=6&amp;list=PL1AEFDC6AC935BAFC	
556	1	Chuo	
557	1	Koraku	
558	1	Elementary Japanese	
559	1	Tasmanians	
560	1	the Platinum 3776	
561	1	Silvexa	
562	1	Aztec	
563	1	J. Herbin 1670	
564	1	the High Ace	
565	1	Newton	
566	1	250ml	
567	1	pelikan M100	
568	1	Amodex	
569	1	Sailor Oku	
570	1	Luciferian	
571	1	Anthracite	
572	1	Instagrams	
573	1	jiffy	
574	1	Orange Jinhao 159s](http://www.ebay.com	
575	1	Jinhao 559	
576	1	Republican	
577	1	Missing-Pen.de	
578	1	Chivor	
579	1	Onoto	
580	1	West German	
581	1	UTF8&amp;dpID=51MAWedqtxL&amp;dpSrc	
582	1	Pelikan M600/800	
583	1	pinkish	
584	1	Keigelu	
585	1	Copperorange	
586	1	Fuckin	
587	1	Western Broad	
588	1	Rembrandt	
589	1	UTF8&amp;qid=1449554455&amp;sr=1-10&amp;keywords	
590	1	Bics	
591	1	featherproof	
592	1	https://youtu.be/lXQZ4wkJwq0	
593	1	Judeo-Christian	
594	1	Ornamental	
595	1	Z26	
596	1	Higgins	
597	1	SN^2	
598	1	Catfish	
599	1	Peikan	
600	1	IJzer	
601	1	the FPR Indus	
602	1	Baoer	
603	1	X450	
604	1	Eastman	
605	1	BROAD	
606	1	Attic	
607	1	http://www.fp-ink.info/en/	
608	1	abliet	
609	1	Jinhao Safari	
610	1	Illini	
611	1	http://www.lacouronneducomte.nl/webstore/main/shipping.php	
612	1	Ohashido	
613	1	Woolf	
614	1	Aspen	
615	1	Pelkian	
616	1	Jinjao	
617	1	bait'n'switch	
618	1	finnish	
619	1	non-Baystate	
620	1	Marine	
621	1	Postman	
622	1	Jowo German	
623	1	Clairefontaine French	
624	1	ink](https://vanness1938.com/?q	
625	1	here](https://www.ebay.de	
626	1	https://imgur.com/2k1tr0e	
627	1	125th	
628	1	gourlet	
629	1	Kanazawa	
630	1	BCHR Watermans	
631	1	Kinmoku	
632	1	Soft Medium	
633	1	Amethyst	
634	1	BB	
635	1	Autograph	
636	1	Dark Souls 3	
637	1	Englishman	
638	1	Bloser	
639	1	Flagfish	
640	1	121~	
641	1	Southern	
642	1	Debutante	
643	1	Whitman	
644	1	Danis	
645	1	site](http://www.papiton.de	
646	1	Tweak	
647	1	Dudek	
648	1	Kwz	
649	1	canadian	
650	1	Titans	
651	1	Iroshizukus	
652	1	Jinhao 1200	
653	1	Pelikan m400	
654	1	Tsukushi	
655	1	https://www.reddit.com/r/fountainpens/comments/4ltg38/53116_handwriting_practice_benchmark/	
656	1	Iroshizuku Take Sumi	
657	1	hangul	
658	1	x250	
659	1	Prera	
660	1	http://imgur.com/kMIYGyz	
661	1	Visctoni	
662	1	Sailorish	
663	1	korean	
664	1	Fuyusyogun	
665	1	Tsuyu	
666	1	nib - 14k	
667	1	Floridians	
668	1	https://www.youtube.com/watch?v=A3ND2P4m0nE	
669	1	Sparklepony	
670	1	swirls	
671	1	Kansai	
672	1	m400	
673	1	Pelikan Silvexa	
674	1	MacBook	
675	1	Images	
676	1	Pukka	
677	1	https://goo.gl/images/fc6vCd	
678	1	Prism	
679	1	Chesterfield	
680	1	Astra	
681	1	FLAK	
682	1	Syrian	
683	1	Luoshi Chinese	
684	1	eurpean	
685	1	UTF8&amp;qid=1473036421&amp;sr=8-1&amp;keywords	
686	1	Levengers	
687	1	Spaceincats	
688	1	Asahiyakami	
689	1	stores](http://www.ampelmann.de	
690	1	Hoween	
691	1	Toscanini	
692	1	21k	
693	1	biologist	
694	1	Hoscal	
695	1	Kyoto	
696	1	M02	
697	1	edit1	
698	1	Kodachrome	
699	1	Tombow	
700	1	Toluline	
701	1	Gaudi	
702	1	non-Montblanc	
703	1	Socrates	
704	1	Spartan	
705	1	a Justus 95	
706	1	Kiwaguro	
707	1	http://imgur.com/a/s7747	
708	1	Ungaro	
709	1	hp+laserjet+24	
710	1	Jellyfish	
711	1	Thriumph	
712	1	Christmassy	
713	1	YouTubers	
714	1	Yoobi	
715	1	Akkermans	
716	1	Pelikan M805	
717	1	Western/European	
718	1	Shops	
719	1	Ticonderoga	
720	1	Inkyman	
721	1	Z50	
722	1	The German Fine	
723	1	Clairefontainte	
724	1	Hans Christian Andersen	
725	1	泳）is	
726	1	LineDict	
727	1	a Clairefontaine French	
728	1	Inklians	
729	1	Fountain	
730	1	dry(er	
731	1	1950s-1960s	
732	1	Montblanc-Winterglow	
733	1	New American	
734	1	Southwest	
735	1	https://imgur.com/a/jgQRW	
736	1	article](http://reviews.shopwritersbloc.com	
737	1	Exotic	
738	1	UTF8&amp;qid=1483908993&amp;sr=8-1&amp;keywords	
739	1	Jinhao 750	
740	1	Barovian	
741	1	Cross	
742	1	Tuscan	
743	1	Fimo	
744	1	;)      	
745	1	non-Visconti	
746	1	cap - Visconti London	
747	1	Patek	
748	1	mans	
749	1	yonge	
750	1	Seremo	
751	1	an All American	
752	1	Penguin	
753	1	Middle Age French	
754	1	Kon	
755	1	largish	
756	1	Platignum	
757	1	MBRG	
758	1	clog	
759	1	Paladium	
760	1	SWAN	
761	1	Neiwans	
762	1	a Morita Sailor	
763	1	~$135-$155	
764	1	Esterbrook Safari	
765	1	Bernake	
766	1	ECO	
767	1	3USD	
768	1	Dark Lilac Safari	
769	1	UTF8&amp;qid=1488887914&amp;sr=1-1&amp;keywords	
770	1	Agatha	
771	1	Jinhao 186	
772	1	Shosaikan	
773	1	Darwinism	
774	1	Endura	
775	1	Asakusa	
776	1	Optima	
777	1	UTF8&amp;qid=1490279371&amp;sr=8	
778	1	CH	
779	1	Hessian	
780	1	Utilitarian	
781	1	Mostly	
782	1	Asians	
783	1	2006/07	
784	1	Galactic	
785	1	Nikaido	
786	1	Vromans	
787	1	Coptic	
788	1	English Duofolds	
789	1	Martians	
790	1	Hitchhiker	
791	1	5pen	
792	1	UTF8&amp;qid=1493083755&amp;sr=8-1&amp;keywords	
793	1	US	
794	1	Western/Japanese	
795	1	Elements	
796	1	KM	
797	1	yr	
798	1	Trident	
799	1	Japanese Pens	
800	1	malaysian	
801	1	Wanikani	
802	1	Alas	
803	1	dry'ish	
804	1	Pelikan(pelikans	
805	1	office&amp;ie	
806	1	nazi	
807	1	iroshizuku asa gao	
808	1	Erig	
809	1	Preras	
810	1	Marines	
811	1	Kokkokai	
812	1	Saskatchewan	
813	1	Submission	
814	1	Oxonian	
815	1	Pensamburo	
816	1	Leuchtturm	
817	1	Bosca	
818	1	ebay.de	
819	1	red-ORANGE	
820	1	Hobonchi	
821	1	African	
822	1	Pelikan m800	
823	1	Appelboom Pennen	
824	1	pageci%253A52670d9b-62b4-11e7-b231-74dbd1804703%257Cparentrq%253A1aae75f515d0ab1c916e965ffffdc4ed%257Ciid%253A1	
825	1	Ceramic	
826	1	Seiz-Kreuznach	
827	1	ins	
828	1	Middle Eastern	
829	1	russian	
830	1	Youtubers	
831	1	a7dfcc52-7556-4594-bd90-4d38e5196008	
832	1	Clear	
833	1	P.E.	
834	1	Sydney	
835	1	Beech	
836	1	Webnotes	
837	1	jinhao x450	
838	1	Sabonis	
839	1	Ancient Chinese	
840	1	mediums	
841	1	Deccan	
842	1	Step	
843	1	Bleach	
844	1	Bengali	
845	1	pt/930	
846	1	Jinhao x450s	
847	1	Fillipino	
848	1	Saffari	
849	1	Flex SIG	
850	1	minis	
851	1	site](www.rhodiapads.com	
852	1	trouvé dans quel	
853	1	XACTO	
854	1	SERIOUSLY	
855	1	non-Chinese	
856	1	Kuro	
857	1	kyobo	
858	1	Modern	
859	1	nib Souverans	
860	1	Kingsize	
861	1	316L	
862	1	Muricans	
863	1	Murican	
864	1	post - Japanese	
865	1	Somers	
866	1	San Giovanni	
867	1	cerneala.eu.they	
868	1	zebraG	
869	1	flexish	
870	1	Con70	
871	1	720p	
872	1	Luxembourgish	
873	1	Scandinavian	
874	1	Cyrillic	
875	1	Pelikan Souverans	
876	1	Rhodium	
877	1	Floral	
878	1	Corian	
879	1	Skyline	
880	1	Windex	
881	1	Lovecraftian	
882	1	Magi	
883	1	QC	
884	1	Iridium	
885	1	Japanese Mediums	
886	1	M Japanese	
887	1	Kandydish	
888	1	JH	
889	1	Metropolotian	
890	1	metropolian	
891	1	Stola	
892	1	\#6	
893	1	Brits	
894	1	UTF8&amp;qid=1513555633&amp;sr=8	
895	1	the Red Resin	
896	1	Cuban	
897	1	742/912	
898	1	Mystique	
899	1	https://www.reddit.com/r/fountainpens/comments/71mafb/gold_992_zebrag_ackerman_trifecta/	
900	1	the Sailor KoP	
901	1	Indians	
902	1	non-English	
903	1	~$70ish	
904	1	m805	
905	1	Non-Cuban	
906	1	Knickerbocker	
907	1	=	
908	1	nib - Jinhao	
909	1	Talentum	
910	1	tintenprobe.de	
911	1	Alkaline	
912	1	Pelikan 400	
913	1	~$60ish	
914	1	pt/719	
915	1	InCoWriMo	
916	1	Medalist	
917	1	Colorverse	
918	1	New Orleanian	
919	1	Pelikan M100	
920	1	europian	
921	1	Opera Master	
922	1	danish	
923	1	mexicans	
924	1	220s	
925	1	https://imgur.com/a/t4DKL	
926	1	/	
927	1	Portuguese	
928	1	http://i.imgur.com/0maV7Vv.gifv	
929	1	some](https://www.gouletpens.com	
930	1	Buddhist	
931	1	De Atrimentis	
932	1	Euro-American	
933	1	Google Japanese	
934	1	a Lamy Safari	
935	1	swedish	
936	1	https://www.amazon.com/NOVUS-7136-Plastic-Polish-Kit/dp/B002UD0GIG/ref=sr_1_2?ie=UTF8&amp;qid=1519599081&amp;sr=8-2&amp;keywords=novus+plastic+polish	
937	1	https://imgur.com/a/TVoAb	
938	1	Puerto Ricans	
939	1	Jews	
940	1	x05	
941	1	Courtesy	
942	1	non-Nighthawk	
943	1	FL	
944	1	Cornish	
945	1	shikiori	
946	1	maxho_B-JKIcRy6f9IvLbKg	
947	1	Venetian	
948	1	blue'ish	
949	1	Jinhao      	
950	1	SUPER	
951	1	Long37	
952	1	Iranian	
953	1	nib Pelikans	
954	1	Sportsman	
955	1	Black	
956	1	pre-Slovenian	
957	1	new Slovenian	
958	1	Fineline	
959	1	Favourite	
960	1	Converters	
961	1	HuntxJinhao	
962	1	Novelty	
963	1	Zanerian	
964	1	Feb	
965	1	Moorman	
966	1	Communist	
967	1	Capitalist	
968	1	Socialist	
969	1	Communism	
970	1	https://loft.omni7.jp/detail/4573154990306	
971	1	Simplo	
972	1	Mocha	
973	1	Repeated	
974	1	north american	
975	1	toooooo	
976	1	M	
977	1	Postcode	
978	1	Stipula Calamo	
979	1	Non-Latin 1	
980	1	Input	
981	1	Carolingian	
982	1	Blueish	
983	1	Sinanian	
984	1	UTF8&amp;qid=1529950261&amp;sr=1-1&amp;keywords	
985	1	non american	
986	1	Pelikan Souveran	
987	1	Metallics	
988	1	Amazonbasics	
989	1	Leviathans	
990	1	Elite	
991	1	Kiwi	
992	1	caese	
993	1	CAN'T	
994	1	Sailor Shikiori	
995	1	DeAtramentis Indian	
996	1	Pietersite	
997	1	Cartesian	
998	1	Artbox	
999	1	Saturates	
1000	1	Pecan	
1001	1	Justus 95	
1002	1	findd	
1003	1	350ish	
1004	1	Jinhao Chinese	
1005	1	Tanzanite	
1006	1	non-Falcon	
1007	1	Copics	
1008	1	penmans	
1009	1	Sailor Nib	
1010	1	Indonesian	
1011	1	Tamenuri	
1012	1	Egyptians	
1013	1	Insta	
1014	1	Baoers	
1015	1	Petit1	
1016	1	europeans	
1017	1	Narodne Novine	
1018	1	Kokkoukai	
1019	1	Crane	
1020	1	Paulownia	
1021	1	Super	
1022	1	non-Masuyama	
1023	1	Greenish	
1024	1	Purplish	
1025	1	Blackish	
1026	1	Serbs	
1027	1	Rymans	
1028	1	non-Realo	
1029	1	~$80ish	
1030	1	South American	
1031	1	el5fV3a	
1032	1	Red(dish	
1033	1	Pilotb	
1034	1	Sailor Realo	
1035	1	R.	
1036	1	Brian	
1037	1	Lemans	
1038	1	piston mech	
1039	1	Slim**Smallish	
1040	1	Sumaura	
1041	1	Philippine	
1042	1	Stylish	
1043	1	849s	
1044	1	Purpurae - Italian Ice	
1045	1	toucans	
1046	1	https://www.penstore.dk/diamine/ink-30ml	
1047	1	style.ca	
