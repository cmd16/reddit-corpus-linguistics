# Data, analysis, and report for corpus linguistics analysis of r/fountainpens

Note: if you're not sure where to start, I recommend either [report.md](report.md) (for the written report) or [Findings.ipynb](Findings.ipynb) (for the quantitative analysis)

## Data
- fountainpens-comments_2010_2018-10_nonewline.csv (aka the main csv) has comments and metadata for all comments on r/fountainpens from its beginning in 2010 until the end of October 2018 (after then data collection was more of a hassle so I stopped). I say "all comments", but I did take a few out because they were deleted or because the body was otherwise somehow empty.
- each .txt file in the main directory is for a subcorpus and has one id per line for the ids of comments that fit in that subcorpus (*id* is a field in the main csv and is a unique identifier)
- LIWC2015 Results (fountainpens-comments_2010_2018-10_nonewline.csv).csv is the main csv plus columns for each category in LIWC
- liwc_df.csv has a row for each comment with a column for *id* and each of the LIWC columns
- [comments_from_archive.ipynb](comments_from_archive.ipynb) shows how I collected the data from the internet

## Analysis files
The folder [stats](stats) contains the files with results of spaCy and VADER analysis using the corpus analysis tool I created. These files can be read easily by a human (except for the keyword files and some of the files in [stats/polarity](stats/polarity)), and they can also be read in by the corpus analysis tool (except for a few files, most notably the [stats/token_fd](stats/token_fd) files).

The folder [small_analysis](small_analysis) contains the same sort of data (minus keywords) for the smaller corpus I first collected (around 20000 comments from 2018 using BigQuery).

## Analysis
- Analyzing brands and colors.ipynb is incomplete because I lost interest
- Calculate Bigrams and Trigrams.ipynb does statistical measures for significance of bigrams and trigrams. I stopped with scores_high because it was taking too long
- [analyzing_corpus.ipynb](analyzing_corpus.ipynb) is the corpus linguistics part of analyzing the main corpus (word frequencies, bigrams, trigrams)
- [comments_analysis.ipynb](comments_analysis.ipynb) is the metadata part (plus polarity from VADER) part of analyzing the corpora, as well as finding samples
- [create_corpora.ipynb](create_corpora.ipynb) hs the corpus linguistics part of analyzing the subcorpora (keyword analysis, bigrams, trigrams)
- [Findings.ipynb](Findings.ipynb) summarizes my main findings
- [LIWC_analysis.ipynb](LIWC_analysis.ipynb) analyzes results from LIWC

Note that the corpus analysis was done borrowing the Corpus class and spacy components from [https://gitlab.com/cmd16/spacy_corpus_linguistics_fanfic](https://gitlab.com/cmd16/spacy_corpus_linguistics_fanfic) (that's what the symlinks are for), so to get code involving the Corpus to run you'd need to set up similar symlinks

## Report
[report.md](report.md)